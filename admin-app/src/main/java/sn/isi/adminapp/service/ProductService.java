package sn.isi.adminapp.service;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.isi.adminapp.dao.IProductRepository;
import sn.isi.adminapp.dto.Product;
import sn.isi.adminapp.exception.EntityNotFoundException;
import sn.isi.adminapp.exception.RequestException;

import sn.isi.adminapp.mapping.ProductMapper;


import java.util.List;
import java.util.Locale;

@Service
public class ProductService {

    private final IProductRepository iProductRepository;
    private final ProductMapper productMapper;
    MessageSource messageSource;

    public ProductService(IProductRepository iProductRepository, ProductMapper productMapper, MessageSource messageSource) {
        this.iProductRepository = iProductRepository;
        this.productMapper = productMapper;
        this.messageSource = messageSource;
    }

    @Transactional(readOnly = true)
    public List<Product> getProduct() {
        return iProductRepository.findAll()
                .stream()
                .map(productMapper::toProduct)
                .toList();
    }

    @Transactional(readOnly = true)
    public Product getProductId(int id) {
        return productMapper.toProduct(iProductRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException(messageSource.getMessage("product.notfound", new Object[]{id},
                                Locale.getDefault()))));
    }
    @Transactional
    public Product createProduct(Product product) {
        return productMapper.toProduct(iProductRepository.save(productMapper.fromProduct(product)));
    }

    @Transactional
    public Product updateProduct(int id, Product product) {
        return iProductRepository.findById(id)
                .map(entity -> {
                    product.setId(id);
                    return productMapper.toProduct(
                            iProductRepository.save(productMapper.fromProduct(product)));
                }).orElseThrow(() -> new EntityNotFoundException(messageSource.getMessage("product.notfound", new Object[]{id},
                        Locale.getDefault())));
    }
    @Transactional
    public void deleteProduct(int id){
        try {
            iProductRepository.deleteById(id);
        } catch (Exception e) {
            throw new RequestException(messageSource.getMessage("product.error-deletion", new Object[]{id},
                    Locale.getDefault()),
                    HttpStatus.CONFLICT);
        }
    }
}

