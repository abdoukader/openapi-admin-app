package sn.isi.adminapp.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.adminapp.dto.Product;
import sn.isi.adminapp.service.ProductService;


import java.util.List;
@RestController
@RequestMapping("/product")
@AllArgsConstructor
public class ProductController {

    private ProductService productService;
    @GetMapping
    public List<Product> getAppProducts() {
        return productService.getProduct();
    }
    @GetMapping("/{id}")
    public Product getProductId(@PathVariable("id") int id) {
        return productService.getProductId(id);
    }
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Product createProduct(@Valid @RequestBody Product product) {
        return productService.createProduct(product);
    }

    @PutMapping("/{id}")
    public Product updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        return productService.updateProduct(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable("id") int id) {
        productService.deleteProduct(id);
    }
}


