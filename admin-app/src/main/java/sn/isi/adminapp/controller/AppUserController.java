package sn.isi.adminapp.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sn.isi.adminapp.dto.AppUser;
import sn.isi.adminapp.service.AppUserService;


import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class AppUserController {

    private AppUserService appUserService;
    @GetMapping
    public List<AppUser> getAppUser() {
        return appUserService.getAppUser();
    }
    @GetMapping("/{id}")
    public AppUser getAppUserId(@PathVariable("id") int id) {
        return appUserService.getAppUserId(id);
    }
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public AppUser createAppUser(@Valid @RequestBody AppUser appUser) {
        return appUserService.createAppUser(appUser);
    }

    @PutMapping("/{id}")
    public AppUser updateAppUser(@PathVariable("id") int id, @RequestBody AppUser appUser) {
        return appUserService.updateAppUser(id, appUser);
    }

    @DeleteMapping("/{id}")
    public void deleteAppUser(@PathVariable("id") int id) {
        appUserService.deleteAppUser(id);
    }
}


